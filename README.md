# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Angular Material 4 used to develop shopping cart
* Version: 1.0.0

### How do I get set up? ###

* Clone the repository with command: git clone https://bitbucket.org/ygkmteam/shoppingcart
* Go to shoppingcart dir.
* install dependencies using npm init
* run tests:ng test
* Deployment instructions: ng build --prod