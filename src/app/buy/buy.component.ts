import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.css']
})
export class BuyComponent implements OnInit {

  constructor() { }
  largeimg = "";
  w=208;
  h=416;
  ngOnInit() {
      if(this.largeimg=="")
          {
              this.largeimg="img1.jpeg";
          }
  }
  changeLargeImg = function(image,w,h)
  {
      this.largeimg = image;
      this.w=w;
      this.h=h;
  }

}
