import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule, MdToolbarModule, MdCardModule, MdInputModule, MdButtonModule } from '@angular/material';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { ShopComponent } from './shop/shop.component';
import { BuyComponent } from './buy/buy.component';
import { SearchPipe } from './search.pipe';
import { AddCartComponent } from './add-cart/add-cart.component';
import 'hammerjs';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    ShopComponent,
    BuyComponent,
    SearchPipe,
    AddCartComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    MdToolbarModule,
    MdCardModule,
    MdInputModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    MdButtonModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    RouterModule.forRoot([
      {
          path : 'home',
          component: HomeComponent
      },
      {
          path : 'signup',
          component: SignupComponent
      },
      {
          path : 'login',
          component: LoginComponent
      },
      {
          path : 'shop',
          component: ShopComponent
      },
      {
          path : 'buy',
          component: BuyComponent
      }
  ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
