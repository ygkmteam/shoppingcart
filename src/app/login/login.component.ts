import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
/* emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)]);
    passwordFormControl = new FormControl('', [Validators.required]); */
    loginform;
    ngOnInit() {
    this.loginform = new FormGroup({
        email : new FormControl('', [
       Validators.required,
       Validators.pattern(EMAIL_REGEX)]),
      password : new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(10)])
    });
    }
    onLogin = function(user)
    {
        console.log("onLogin", user);
    }
      constructor() { }
}