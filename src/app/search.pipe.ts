import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

    transform(mobiles: any[], term): any {
        console.log('term', term);
        return term ? mobiles.filter(mobile => mobile.name.toLowerCase().indexOf(term.toLowerCase()) !== -1)  : mobiles;
    }
}
