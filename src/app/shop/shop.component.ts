import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import { SearchPipe } from '../search.pipe';
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  constructor(private router: Router) {}
  term;
  mobiles = [
             {
                 "id":1,
                  "name":"Samsung Galaxy J7 Prime (Gold 32 GB) (3 GB RAM)",
                  "price":"15000",
                  "rating":"4.8",
                  "rating_text":"500 Ratings & 81 Reviews",
                  "price_desc":["No Cost EMIs from 2,249 per month","Other EMIs from 655 per month","Get upto 12,500 off on exchange","Special PriceGet extra ₹910 off price inclusive of discount"],
                  "heilights":["3 GB RAM | 32 GB ROM | Expandable Upto 256 GB",
                             "5 inch HD Display",
                             "13MP Rear Camera | 5MP Front Camera",
                             "2400 mAh Li-Ion Battery",
                             "Exynos 7570 Quad Core 1.4GHz Processor"
                             ],
                 "sellers":["Seller","View 14 sellers starting from 13,490"],
                 "thumbs":["sam1","sam2","sam3","sam4"],
                 "main_image" : "sam1.png",
                 "main_images" : ["sam1","sam2","sam3","sam4"]
              },
              {
                  "id":2,
                  "name":"OPPO A37f (Grey 16 GB)  (2 GB RAM)",
                  "price":"9900",
                  "rating":"4.1",
                  "rating_text":"5,147 Ratings & 1,291 Reviews",
                  "price_desc":["No Cost EMIs from 833/month", "Other EMIs from 485/month","Bank OfferExtra 5% off on Axis Bank Buzz Credit Cards","Partner OfferJio - 90days UNLIMITED 399. ALSO GET UPTO 42GB EXTRA 4G DATA and JIO PRIME across 6 recharges"],
                  "heilights":["2 GB RAM | 16 GB ROM | Expandable Upto 128 GB",
                             "5 inch HD Display",
                             "8MP Rear Camera | 5MP Front Camera",
                             "2630 mAh Li-Ion Battery",
                             "Qualcomm Snapdragon 410 Quad Core 1.2GHz Processor"
                             ],
                 "sellers":["10 Days Replacement Policy"],
                 "thumbs":["oppo1","oppo2","oppo3","oppo4","oppo5"],
                 "main_image" : "oppo1.png",
                 "main_images" : ["oppo1","oppo2","oppo3","oppo4","oppo5"]
              },
              {
                  "id":3,
                  "name":"VIVO V5s Perfect Selfie (Matte Black 64 GB) (4 GB RAM)",
                  "price":"17,990",
                  "rating":"4.3",
                  "rating_text":"6,309 Ratings & 1,354 Reviews",
                  "price_desc":["No Cost EMIs from 1,500/month. Other EMIs from 873 per month","Get upto 17,000 off on exchange","Extra Rs.1000 Off on Regular Exchange"],
                  "heilights":["4 GB RAM | 64 GB ROM | Expandable Upto 256 GB",
                             "5.5 inch HD Display",
                             "13MP Rear Camera | 20MP Front Camera",
                             "3000 mAh Battery",
                             "MediaTek MT6750 64-bit Octa Core 1.5GHz Processor",
                             "1.5 GHz Quad Core"
                             ],
                 "sellers":["10 Days Replacement Policy"],
                " thumbs":["vivo1","vivo2","vivo3","vivo4"],
                 "main_image" : "vivo1.png",
                 "main_images" : ["vivo1","vivo2","vivo3","vivo4"]
              }
             ];
  ngOnInit()
  {
      
  }
  onLogin = function(user)
  {
      console.log("onLogin", user);
  }
  toBuy = function()
  {
      this.router.navigateByUrl('/buy');
  }
}
